package model;

import db.SqliteDatabaseProvider;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class StatsCalculator {


    private double totalOnboardPaid;
    private double totalontrainingPaid;
    private double totalOnleavePaid;

    private double totalBonusesPaid;

    private static StatsCalculator instance;
    private SqliteDatabaseProvider sqliteDatabaseProvider = SqliteDatabaseProvider.getInstance();

    private StatsCalculator() {



    }

    public static StatsCalculator getInstance() {

        if (instance == null)  instance = new StatsCalculator();

        return instance;
    }

    public void addAmmountOnBoard(int seamanId, LocalDate date, double amount) {

        totalOnboardPaid += amount;
        sqliteDatabaseProvider.updateDailyAmount(seamanId, date, amount);
    }

    public void addAmmountOnTraining(int seamanId, LocalDate date, double amount) {

        totalontrainingPaid += amount;
        sqliteDatabaseProvider.updateDailyAmount(seamanId, date, amount);

    }

    public void addAmmountOnLeave(int seamanId, LocalDate date, double amount) {

        totalOnleavePaid += amount;
        sqliteDatabaseProvider.updateDailyAmount(seamanId, date, amount);

    }

    public void addToTotalBonus(double amount) {
        totalBonusesPaid += amount;
    }

    public double getTotalBonusesPaid() {
        return totalBonusesPaid;
    }

    public Map<String, Double> getAverages() {
        Map<String, Double> averages = new HashMap<>();
        averages.put("MASTER", sqliteDatabaseProvider.getAveragePayment("MASTER"));
        averages.put("CHIEF_OFFICER", sqliteDatabaseProvider.getAveragePayment("CHIEF_OFFICER"));
        averages.put("SECOND_OFFICER", sqliteDatabaseProvider.getAveragePayment("SECOND_OFFICER"));
        averages.put("AB", sqliteDatabaseProvider.getAveragePayment("AB"));
        averages.put("CHIEF_ENGINEER", sqliteDatabaseProvider.getAveragePayment("CHIEF_ENGINEER"));
        averages.put("SECOND_ENGINEER", sqliteDatabaseProvider.getAveragePayment("SECOND_ENGINEER"));
        averages.put("COOK", sqliteDatabaseProvider.getAveragePayment("COOK"));
        averages.put("MESSMAN", sqliteDatabaseProvider.getAveragePayment("MESSMAN"));
        return averages;
    }


    public double getTotalOnboardPaid() {
        return totalOnboardPaid;
    }

    public double getTotalontrainingPaid() {
        return totalontrainingPaid;
    }

    public double getTotalOnleavePaid() {
        return totalOnleavePaid;
    }
}
