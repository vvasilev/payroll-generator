package model;


import java.time.LocalDate;

public class Record {
    private int seamanId;

    private String firstName;

    private String lastName;

    private Rank rank;

    private LocalDate date;

    private SeamanLocation seamanLocation;


    public Record() {
    }

    public int getSeamanId() {
        return seamanId;
    }

    public void setSeamanId(int seamanId) {
        this.seamanId = seamanId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public SeamanLocation getSeamanLocation() {
        return seamanLocation;
    }

    public void setSeamanLocation(SeamanLocation seamanLocation) {
        this.seamanLocation = seamanLocation;
    }

    public enum Rank {
        MASTER,
        CHIEF_OFFICER,
        SECOND_OFFICER,
        AB,
        CHIEF_ENGINEER,
        SECOND_ENGINEER,
        COOK,
        MESSMAN;

        public static Rank fromString(String s) {
            switch (s) {
                case "Master":
                    return Rank.MASTER;
                case "Chief Officer":
                    return Rank.CHIEF_OFFICER;
                case "Second Officer":
                    return Rank.SECOND_OFFICER;
                case "AB":
                    return Rank.AB;
                case "Chief Engineer":
                    return Rank.CHIEF_ENGINEER;
                case "Second Engineer ":
                    return SECOND_ENGINEER;
                case "Cook":
                    return COOK;
                case "Messman":
                    return MESSMAN;
                default:
                    return null;
            }

        }

    }

    public enum SeamanLocation {
        ONBOARD, ONTRAINING, ONLEAVE;

        public static SeamanLocation fromString(String s) {
            switch (s) {
                case "ONBOARD":
                    return ONBOARD;
                case "ONTRAINING":
                    return ONTRAINING;
                case "ONLEAVE":
                    return ONLEAVE;
                default:
                    return null;
            }
        }
    }



}
