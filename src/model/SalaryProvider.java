package model;

public class SalaryProvider {

    private static SalaryProvider instance;

    private static final int MASTER = 0;
    private static final int CHIEF_OFFICER = 1;
    private static final int SECOND_OFFICER = 2;
    private static final int AB = 3;
    private static final int CHIEF_ENGINEER = 4;
    private static final int SECOND_ENGINEER = 5;
    private static final int COOK = 6;
    private static final int MESSMAN = 7;

    private static Double[][] salaryMatrix = new Double[8][3];

    public static SalaryProvider getInstance() {
        if (instance == null) instance = new SalaryProvider();

        return instance;
    }

    private SalaryProvider() {

        // Master
        salaryMatrix[0][0] = 300.0; //  On board
        salaryMatrix[0][1] = 280.0; //  On training
        salaryMatrix[0][2] = 150.0; //  On leave

        // Chief officer
        salaryMatrix[1][0] = 200.0; // On board
        salaryMatrix[1][1] = 170.0; // On training
        salaryMatrix[1][2] = 100.0; // On leave

        // Second Officer
        salaryMatrix[2][0] = 150.0; // On onboard
        salaryMatrix[2][1] = 120.0; // On training
        salaryMatrix[2][2] = 75.0; // On leave

        // AB
        salaryMatrix[3][0] = 70.0; // On onboard
        salaryMatrix[3][1] = 50.0; // On training
        salaryMatrix[3][2] = 35.0; // On leave

        // Chief Engineer
        salaryMatrix[4][0] = 200.0; // On onboard
        salaryMatrix[4][1] = 170.0; // On training
        salaryMatrix[4][2] = 100.0; // On leave

        // Second Engineer
        salaryMatrix[5][0] = 150.0; // On onboard
        salaryMatrix[5][1] = 120.0; // On training
        salaryMatrix[5][2] = 75.0; // On leave

        // Cook
        salaryMatrix[6][0] = 80.0; // On onboard
        salaryMatrix[6][1] = 60.0; // On training
        salaryMatrix[6][2] = 40.0; // On leave

        // Messman
        salaryMatrix[7][0] = 50.0; // On onboard
        salaryMatrix[7][1] = 30.0; // On training
        salaryMatrix[7][2] = 25.0; // On leave

    }

    public static double getSalary(Record.Rank rank, Record.SeamanLocation location) {
        int row;
        int column;

        switch (rank) {

            case MASTER:
                row = MASTER;
                break;
            case CHIEF_OFFICER:
                row = CHIEF_OFFICER;
                break;
            case SECOND_OFFICER:
                row = SECOND_OFFICER;
                break;
            case AB:
                row = AB;
                break;
            case CHIEF_ENGINEER:
                row = CHIEF_ENGINEER;
                break;
            case SECOND_ENGINEER:
                row = SECOND_ENGINEER;
                break;
            case COOK:
                row = COOK;
                break;
            case MESSMAN:
                row = MESSMAN;
                break;
            default:
                throw new IllegalArgumentException();
        }

        switch (location) {
            case ONBOARD:
                column = 0;
                break;
            case ONTRAINING:
                column = 1;
                break;
            case ONLEAVE:
                column = 2;
                break;
            default:
                throw new IllegalArgumentException();
        }

        return salaryMatrix[row][column];
    }
}
