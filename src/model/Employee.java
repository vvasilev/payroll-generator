package model;

import java.time.LocalDate;

public class Employee {

    private static final double BONUS = 0.2;  //  20%

    private int seamanId;
    private String firstName;
    private String lastName;

    private double totalSalary;
    private double totalBonusEarned;
    private int daysOnBoard;
    private int daysOntraining;
    private int daysOnLeave;

    private static SalaryProvider salaryProvider = SalaryProvider.getInstance();

    public Employee() {
    }

    public int getSeamanId() {
        return seamanId;
    }

    public void setSeamanId(int seamanId) {
        this.seamanId = seamanId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getTotalSalary() {
        return totalSalary;
    }

    public double getTotalBonusEarned() {
        return totalBonusEarned;
    }

    public int getTotalDaysOnBoard() {
        return daysOnBoard;
    }

    public int getTotalDaysOnTraining() {
        return daysOntraining;
    }

    public int getDaysOnLeave() {
        return daysOnLeave;
    }



    public void addDay(Record.Rank rank, LocalDate date, Record.SeamanLocation location) {
        switch (location) {
            case ONBOARD:
                daysOnBoard++;
                break;
            case ONTRAINING:
                daysOntraining++;
                break;
            case ONLEAVE:
                daysOnLeave++;
                break;
        }

        double daypay;

        if (shouldReceiveBonus(location)) {
            daypay = salaryProvider.getSalary(rank, location) * (BONUS + 1);  //  0.2 + 1 = 1.2 <=> 120%
            totalSalary += daypay;

            double bonusAdded = salaryProvider.getSalary(rank, location) * BONUS;
            totalBonusEarned += bonusAdded;
            StatsCalculator.getInstance().addToTotalBonus(bonusAdded);
        } else {
            daypay = salaryProvider.getSalary(rank, location);  //  no totalBonusEarned
            totalSalary += daypay;
        }


        //  again, after daypay is calculated
        switch (location) {
            case ONBOARD:
                StatsCalculator.getInstance().addAmmountOnBoard(this.seamanId, date, daypay);
                break;
            case ONTRAINING:
                StatsCalculator.getInstance().addAmmountOnTraining(this.seamanId, date, daypay);
                break;
            case ONLEAVE:
                StatsCalculator.getInstance().addAmmountOnLeave(this.seamanId, date, daypay);
                break;
        }





    }

    private boolean shouldReceiveBonus(Record.SeamanLocation location) {
        return (daysOnBoard > 20) && (location == Record.SeamanLocation.ONBOARD);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Employee) {
            return this.seamanId == ((Employee) o).seamanId;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return seamanId;
    }
}
