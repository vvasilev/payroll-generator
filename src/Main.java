import db.CsvParser;
import db.SqliteDatabaseProvider;
import db.contract.DatabaseProvider;
import model.Employee;
import model.Record;
import model.StatsCalculator;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static String CSV_PATH = "./input/2018-SOFTimpact-SeaniqueCodeTest-MonthlyReport.csv";


    public static void main(String[] args) {
        List<Record> records = new CsvParser(CSV_PATH).getRecords();

        DatabaseProvider dbProvider = SqliteDatabaseProvider.getInstance();
        dbProvider.setRecords(records);

        processEmployees(records);

    }

    static void processEmployees(List<Record> records) {
        List<Employee> employees = new ArrayList<>();

        for (Record record : records) {
            Employee employee = new Employee();
            employee.setSeamanId(record.getSeamanId());
            employee.setFirstName(record.getFirstName());
            employee.setLastName(record.getLastName());

            if (employees.contains(employee)) {
                Employee existingEmployee = employees.get(employees.indexOf(employee));
                employee = existingEmployee;
            } else {

                employees.add(employee);
            }

            employee.addDay(record.getRank(), record.getDate(), record.getSeamanLocation());

        }

        System.out.println();
        System.out.println();

        System.out.println("========= Seafarer Payslips =========\n");

        for (Employee employee : employees) {

            System.out.println("Seafarer ID: " + employee.getSeamanId());
            System.out.println("First name: " + employee.getFirstName());
            System.out.println("Last name: " + employee.getLastName());
            System.out.println("Total salary: " + employee.getTotalSalary());
            System.out.println("Total bonus earned: " + employee.getTotalBonusEarned());
            System.out.println("Total days on board: " + employee.getTotalDaysOnBoard());
            System.out.println("Total days on training: " + employee.getTotalDaysOnTraining());
            System.out.println("Total days on leave: " + employee.getDaysOnLeave());
            System.out.println("====================================\n\n\n");


        }


        //  Print payment

        System.out.println("========= End payment Information (for all seafarers) =========\n");
        StatsCalculator.getInstance().getAverages().toString();

        System.out.println("\n");

        System.out.println("Total amount paid for ONBOARD location: " + StatsCalculator.getInstance().getTotalOnboardPaid());
        System.out.println("Total amount paid for ONTRAINING location: " + StatsCalculator.getInstance().getTotalontrainingPaid());
        System.out.println("Total amount paid for ONLEAVE location: " + StatsCalculator.getInstance().getTotalOnleavePaid());

        System.out.println("\n");

        System.out.println("Total amount paid for seafarer bonuses: " + StatsCalculator.getInstance().getTotalBonusesPaid());
    }
}
