package db;

import db.contract.DatabaseProvider;
import model.Record;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;

public class SqliteDatabaseProvider implements DatabaseProvider {

    private Connection connection;

    private static SqliteDatabaseProvider instance;

    private SqliteDatabaseProvider() {
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:db.sqlite");
            createTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static SqliteDatabaseProvider getInstance() {
        if (instance == null) instance = new SqliteDatabaseProvider();

        return instance;
    }

    private void createTable() throws SQLException {
        Statement statement = connection.createStatement();

        String sql = "CREATE TABLE IF NOT EXISTS seamen (" +
                "id INTEGER PRIMARY KEY, " +
                "seaman_id INTEGER NOT NULL, " +
                "first_name VARCHAR(255), " +
                "last_name VARCHAR(255), " +
                "rank VARCHAR(255), " +
                "date VARCHAR(255), " +
                "location VARCHAR(255)," +
                "daily_payment REAL)";
        statement.executeUpdate(sql);
    }

    @Override
    public void setRecords(List<Record> records) {
        try {

            for (Record record : records) {
                Statement statement = connection.createStatement();

                String sql = "INSERT OR REPLACE INTO seamen (seaman_id, first_name, last_name, rank, date, location) VALUES(" +
                        "'" + record.getSeamanId() + "', " +
                        "'" + record.getFirstName() + "', " +
                        "'" + record.getLastName() + "', " +
                        "'" + record.getRank() + "', " +
                        "'" + record.getDate().toString() + "', " +
                        "'" + record.getSeamanLocation().toString() +
                        "')";
                statement.executeUpdate(sql);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateDailyAmount(int seamanId, LocalDate date, double amount) {
        try {
//            Statement statement = connection.createStatement();
            String sql = "UPDATE seamen" +
                    " SET daily_payment = ? " +
                    " WHERE " +
                    "    seaman_id =  ? " +
                    "    AND date = ? ";
//            statement.executeUpdate(sql);

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDouble(1, amount);
            preparedStatement.setInt(2, seamanId);
            preparedStatement.setString(3, date.toString());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Double getAveragePayment(String rank) {
        String sql = "select avg(daily_payment) " +
                "from seamen " +
                "where rank = \""+ rank + "\" ";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println();
            while ((resultSet.next())) {
                System.out.println("Average salary paid per day of the " + rank + " is:  " + resultSet.getString("avg(daily_payment)"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


}
