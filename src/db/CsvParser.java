package db;

import com.opencsv.CSVReader;
import db.contract.DbContract;
import model.Record;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CsvParser {
    private String path;


    public CsvParser(String path) {
        this.path = path;
    }

    private Reader readFile(String path) throws IOException {
        return Files.newBufferedReader(Paths.get(path));
    }


    public List<Record> getRecords() {
        List<Record> records = new ArrayList<>();
        try {
            CSVReader csvReader = new CSVReader(readFile(path), ',', '\'', 1);

            String[] nextRecord;
            while ((nextRecord = csvReader.readNext()) != null) {

                Record record = new Record();

                record.setSeamanId(Integer.valueOf(nextRecord[DbContract.CsvContract.ID]));
                record.setFirstName(nextRecord[DbContract.CsvContract.FIRST_NAME]);
                record.setLastName(nextRecord[DbContract.CsvContract.LAST_NAME]);
                record.setRank(Record.Rank.fromString(nextRecord[DbContract.CsvContract.RANK]));
                record.setDate(parseDate(nextRecord[DbContract.CsvContract.DATE]));
                record.setSeamanLocation(Record.SeamanLocation.fromString(nextRecord[DbContract.CsvContract.LOCATION]));

                records.add(record);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }

    private static LocalDate parseDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        return LocalDate.parse(date, formatter);
    }

}
