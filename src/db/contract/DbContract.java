package db.contract;

public abstract class DbContract {

    public abstract class CsvContract {
        public static final int ID = 0;
        public static final int FIRST_NAME = 1;
        public static final int LAST_NAME = 2;
        public static final int RANK = 3;
        public static final int DATE = 4;
        public static final int LOCATION = 5;
    }
}
