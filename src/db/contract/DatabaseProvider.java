package db.contract;

import model.Record;

import java.time.LocalDate;
import java.util.List;

public interface DatabaseProvider {


    public void setRecords(List<Record> records);

    public void updateDailyAmount(int seamanId, LocalDate date, double ammount);

}
