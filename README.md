# Payroll Generator

SoftImpact Challenge: Calcullating the payroll of a vassel'screw every month. At the end of each month the captain provides a CSV file of which seafarers were on-board, training or on-leave. Depending on their location and their rank the sefarers receive the appropriate pay for the month.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
IDE: Recommended -> IntelliJ IDEA or any similar to it (i.e. Eclipse)

```

Download IntelliJ from [here](https://www.jetbrains.com/idea/download/#section=mac).



### Installing

A step by step series of examples how to run the payroll-generator.

```
1) git clone the repo or downlaod the zip file
2) open IntelliJ and open the cloned folder
3) add the jar files from the lib folder
	1) File -> Project Structure -> Libraries
	2) Press the '+' button at the bottom
	3) Go to thwe lib folder at ./softimpact-payroll->lib
	4) Select all jar files
	5) press open
	6) At the bottom of the window press APPLY and then OK
```


## Running the app

Explain how to run the automated tests for this system

```
1) Select Edit Configurations:
	1) In the 'Main Class' filed choose Main
	2) In the 'Working Directory' filed make sure is the correct path to the 'softimpact-payroll'

2) Make sure you are using JRE 1.8 - SDK
```

## Built With

* SQLite - The database 
* Java - The Programming Language
* OpenCSV - Used to read the CSV File


## Author

* **Vasil Vasilev** - *Software Engineer* - [vasco](https://vvasilev.eu)



